package Selenium;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 * @author scucc
 */
public class TestHome {

    public static String browser = "Chrome"; // External configuration - XLS, CSV
    public static WebDriver driver;

    /**
     *
     */
    @Test
    public void TestUI() {

        if (browser.equals("Firefox")) {
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();
        } else if (browser.equals("Chrome")) {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
        }

        driver.get("http://localhost:8080/ProgettoISA/Dispatcher");
        driver.manage().window().maximize();
        driver.findElement(By.id("username")).sendKeys("simone");
        driver.findElement(By.id("password")).sendKeys("simone");
        driver.findElement(By.id("login")).click();
        driver.findElement(By.name("userArea")).click();

        String info = driver.getCurrentUrl();
        System.out.println("url corrente " + info);

        info = driver.getTitle();
        System.out.println("title corrente " + info);

        info = driver.getPageSource();
        System.out.println("codice sorgente corrente:/n" + info);

    }

}
