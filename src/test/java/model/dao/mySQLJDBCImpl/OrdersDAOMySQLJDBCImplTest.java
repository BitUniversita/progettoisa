/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao.mySQLJDBCImpl;

import java.sql.Connection;
import java.sql.ResultSet;

import java.util.List;
import model.mo.Product;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Disabled;
import static org.mockito.Mockito.*;
import services.config.Configuration;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.dao.exception.DuplicatedObjectException;
import model.mo.Orders;

/**
 *
 * @author scucc
 */
public class OrdersDAOMySQLJDBCImplTest {

    Connection conn;

    public OrdersDAOMySQLJDBCImplTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of findByOrder_Id method, of class OrdersDAOMySQLJDBCImpl.
     */
    @Test
    public void findOrdersByOrder_Id() throws SQLException {

        System.out.println("test findByOrder_ID");
        conn = DriverManager.getConnection(Configuration.DATABASE_URL);
        Long order_Id = Long.valueOf(1);
        OrdersDAOMySQLJDBCImpl instance = new OrdersDAOMySQLJDBCImpl(conn);
        Orders expResult = new Orders();
        expResult.setBuyer("simone");
        expResult.setTotprice(1500);
        expResult.setStatus("elaborazione");
        Orders result = instance.findOrdersByOrder_Id(order_Id);
        System.out.println("se trova simone allora il test findByOrder_ID funziona!:  " + result.getBuyer());  //prova per vedere se il test funziona
        assertEquals(expResult.getBuyer(), result.getBuyer());
        assertEquals(expResult.getTotprice(), result.getTotprice());
        assertEquals(expResult.getStatus(), result.getStatus());

    }

    @Test
    public void insert() throws DuplicatedObjectException, SQLException {

        System.out.println("test insert order");
        conn = DriverManager.getConnection(Configuration.DATABASE_URL);
        OrdersDAOMySQLJDBCImpl instance = new OrdersDAOMySQLJDBCImpl(conn);
        Orders expResult = new Orders();
        expResult.setBuyer("simone");
        expResult.setTotprice(Float.valueOf("1580"));
        expResult.setDescription("prodID:4 Qty:1");
        Orders result = instance.insert(expResult.getBuyer(), expResult.getDescription(), expResult.getTotprice());
        assertEquals(expResult.getBuyer(), result.getBuyer());
        assertEquals(expResult.getTotprice(), result.getTotprice());
        System.out.println("se " + result.getBuyer() + " è uguale a " + expResult.getBuyer() + "  allora il test insert funziona!");  //prova per vedere se il test funziona

    }
    /*
    @Test
    public void delete() throws DuplicatedObjectException{
        try {
            System.out.println("test delete order");
            conn = DriverManager.getConnection(Configuration.DATABASE_URL);
            OrdersDAOMySQLJDBCImpl instance = new OrdersDAOMySQLJDBCImpl(conn);
            Orders order = new Orders();
            order.setBuyer("test");
            order.setTotprice(Float.valueOf("520"));
            order.setDescription("prodID:4 Qty:1");
            Orders res = instance.insert(order.getBuyer(),order.getDescription(),order.getTotprice());
            System.out.println("inserito ordine con status: " + res.getStatus());  //prova per vedere se il test arriva qui
            int result;
            
            result = instance.delete(res); bisogna modificare la delete 
            int expResult = 1;
            assertEquals(expResult, result);
            System.out.println("se " + expResult + " = 1 allora il test delete funziona!");  //prova per vedere se il test funziona
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAOMySQLJDBCImplTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }*/
}
