/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao.mySQLJDBCImpl;

import java.sql.Connection;
import java.sql.ResultSet;

import java.util.List;
import model.mo.Product;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Disabled;
import static org.mockito.Mockito.*;
import services.config.Configuration;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.mo.PushedProduct;

/**
 *
 * @author scucc
 */
public class PushedProductDAOMySQLJDBCImplTest {
    Connection conn;
    
    public PushedProductDAOMySQLJDBCImplTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    
    /**
     * Test of findByProdId method, of class PushedProductDAOMySQLJDBCImpl.
     */
    
    @Test
    public void testFindByProdId() throws SQLException{
        
            System.out.println("test findByProdId");
            conn = DriverManager.getConnection(Configuration.DATABASE_URL);
            Long prod_Id = Long.valueOf(11);
            PushedProductDAOMySQLJDBCImpl instance = new PushedProductDAOMySQLJDBCImpl(conn);
            Product expResult = new Product();
            expResult.setBrand("GoPro");
            expResult.setModel("Karma");
            expResult.setPrice(Float.valueOf("419"));
            PushedProduct result = instance.findByProdId(prod_Id);
            System.out.println("se trova GoPro allora il test findByProdID funziona!:  " + result.getBrand());  //prova per vedere se il test funziona
            assertEquals(expResult.getBrand(), result.getBrand());
            assertEquals(expResult.getModel(), result.getModel());
            assertEquals(expResult.getPrice(), result.getPrice());
    } 
}