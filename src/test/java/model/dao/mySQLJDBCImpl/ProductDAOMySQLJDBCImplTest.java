/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao.mySQLJDBCImpl;

import java.sql.Connection;
import java.sql.ResultSet;

import java.util.List;
import model.mo.Product;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Disabled;
import static org.mockito.Mockito.*;
import services.config.Configuration;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author scucc
 */
public class ProductDAOMySQLJDBCImplTest {

    Connection conn;

    public ProductDAOMySQLJDBCImplTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of findByProdId method, of class ProductDAOMySQLJDBCImpl.
     */
    @Test
    public void testFindByProdId() throws SQLException {

        System.out.println("test findByProdId");
        conn = DriverManager.getConnection(Configuration.DATABASE_URL);
        Long prod_Id = Long.valueOf(1);
        ProductDAOMySQLJDBCImpl instance = new ProductDAOMySQLJDBCImpl(conn);
        Product expResult = new Product();
        expResult.setBrand("DJI");
        expResult.setModel("Inspire1Pro");
        expResult.setPrice(Float.valueOf("3000"));
        Product result = instance.findByProdId(prod_Id);
        System.out.println("se trova DJI allora il test findByProdID funziona!:  " + result.getBrand());  //prova per vedere se il test funziona
        assertEquals(expResult.getBrand(), result.getBrand());
        assertEquals(expResult.getModel(), result.getModel());
        assertEquals(expResult.getPrice(), result.getPrice());

    }

    /**
     * Test of findByModel method, of class ProductDAOMySQLJDBCImpl.
     */
    @Test
    public void testFindByModel() throws SQLException {

        System.out.println("test findByModel");
        conn = DriverManager.getConnection(Configuration.DATABASE_URL);
        ProductDAOMySQLJDBCImpl instance = new ProductDAOMySQLJDBCImpl(conn);
        Product expResult = new Product();
        expResult.setBrand("DJI");
        expResult.setModel("Inspire1Pro");
        expResult.setPrice(Float.valueOf("3000"));
        Product result = instance.findByModel(expResult.getModel());
        System.out.println("se trova DJI allora il test findByModel funziona!:  " + result.getBrand());  //prova per vedere se il test funziona
        assertEquals(expResult.getBrand(), result.getBrand());
        assertEquals(expResult.getModel(), result.getModel());
        assertEquals(expResult.getPrice(), result.getPrice());

    }

    /**
     * Test of findByQty method, of class ProductDAOMySQLJDBCImpl.
     */
    @Test
    public void testFindByQty() throws SQLException {

        System.out.println("test FindByQty");
        conn = DriverManager.getConnection(Configuration.DATABASE_URL);
        ProductDAOMySQLJDBCImpl instance = new ProductDAOMySQLJDBCImpl(conn);
        Product expResult = new Product();
        expResult.setBrand("DJI");
        expResult.setModel("Inspire1Pro");
        expResult.setPrice(Float.valueOf("3000"));
        expResult.setQty(Long.valueOf("5"));
        Product result = instance.findByQty(expResult.getQty());
        System.out.println("se trova Inspire1Pro allora il test findByQty funziona!:  " + result.getModel());  //prova per vedere se il test funziona
        assertEquals(expResult.getBrand(), result.getBrand());
        assertEquals(expResult.getModel(), result.getModel());
        assertEquals(expResult.getPrice(), result.getPrice());

    }

    /**
     * Test of findByPrice method, of class ProductDAOMySQLJDBCImpl.
     */
    @Test
    public void testFindByPrice() throws SQLException {

        System.out.println("test FindByPrice");
        conn = DriverManager.getConnection(Configuration.DATABASE_URL);
        ProductDAOMySQLJDBCImpl instance = new ProductDAOMySQLJDBCImpl(conn);
        Product expResult = new Product();
        expResult.setBrand("DJI");
        expResult.setModel("Inspire1Pro");
        expResult.setPrice(Float.valueOf("3000"));
        Product result = instance.findByPrice(expResult.getPrice());
        System.out.println("se trova DJI allora il test findByPrice funziona!:  " + result.getBrand());  //prova per vedere se il test funziona
        assertEquals(expResult.getBrand(), result.getBrand());
        assertEquals(expResult.getModel(), result.getModel());
        assertEquals(expResult.getPrice(), result.getPrice());

    }

}
